import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.*;

public class AddTab extends JPanel implements ActionListener {
    JTextField name, phone, email, dob;
    JButton submit;
    GUI obj;

    public AddTab(GUI obj) {
        this.obj = obj;

        add(new JLabel("Enter your name:"));
        name = new JTextField(15);
        add(name);

        add(new JLabel("Enter your phone number:"));
        phone = new JTextField(15);
        add(phone);

        add(new JLabel("Enter your email:"));
        email = new JTextField(15);
        add(email);

        add(new JLabel("Enter your DOB:"));
        dob = new JTextField(15);
        add(dob);

        submit = new JButton("SUBMIT");
        submit.setEnabled(false);
        submit.addActionListener(this);
        add(submit);

        // Add a document listener to the name field
        name.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                toggleSubmitButton();
            }

            public void removeUpdate(DocumentEvent e) {
                toggleSubmitButton();
            }

            public void insertUpdate(DocumentEvent e) {
                toggleSubmitButton();
            }

            private void toggleSubmitButton() {
                submit.setEnabled(!name.getText().trim().isEmpty());
            }
        });
    }

    public void actionPerformed(ActionEvent ae) {
        ContactsList.addContact(new Contact(name.getText(), phone.getText(), email.getText(), dob.getText()));
        JOptionPane.showMessageDialog(this, "Your contact was successfully added!", "SUCCESS!",
                JOptionPane.INFORMATION_MESSAGE);
        obj.updateContactsTab();
        name.setText("");
        phone.setText("");
        email.setText("");
        dob.setText("");
        submit.setEnabled(false); // Disable the button again until name is filled
    }
}
