import java.awt.BorderLayout;
import javax.swing.*;
import java.awt.event.*;

public class DeleteTab extends JPanel implements ActionListener, ItemListener {
  JComboBox<String> names;
  JButton delete;
  JTextField name, phone, email, dob;
  GUI obj;

  DeleteTab(GUI obj) {
    this.obj = obj;
    add(new JLabel("Delete a contact by Name"), BorderLayout.CENTER);

    names = new JComboBox<>();
    for (int i = 0; i < ContactsList.getSize(); i++) {
      names.addItem(ContactsList.getName(i));
    }

    add(names);

    add(new JLabel("Name:"));
    add(name = new JTextField(15));
    add(new JLabel("Phone Number:"));
    add(phone = new JTextField(15));
    add(new JLabel("Email:"));
    add(email = new JTextField(15));
    add(new JLabel("DOB:"));
    add(dob = new JTextField(15));

    add(delete = new JButton("DELETE"));
    delete.addActionListener(this);
    names.addItemListener(this);
  }

  public void actionPerformed(ActionEvent ae) {
    String deleteData = names.getSelectedItem().toString();
    if (deleteData == "") {
      JOptionPane.showMessageDialog(this, "Please select a contact to be deleted", "ERROR!", JOptionPane.ERROR_MESSAGE);

    } else {
      ContactsList.deleteContact(deleteData);
      JOptionPane.showMessageDialog(this, "Contact Deleted Successfully!", "SUCCESS!", JOptionPane.INFORMATION_MESSAGE);
      obj.updateContactsTab();
      name.setText("");
      phone.setText("");
      email.setText("");
      dob.setText("");
    }
  }

  public void itemStateChanged(ItemEvent ie) {
    Contact c = ContactsList.getContact(names.getSelectedIndex());
    name.setText(c.getName());
    phone.setText(c.getPhoneNumber());
    email.setText(c.getEmail());
    dob.setText(c.getDOB());
  }
}
