import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.event.*;

public class UpdateTab extends JPanel implements ActionListener, ItemListener {
  JComboBox<String> names;
  JTextField name, phone, email, dob;
  JButton update;
  GUI obj;
  String deletedData;

  UpdateTab(GUI obj) {
    this.obj = obj;
    add(new JLabel("Update a contact by Name"), BorderLayout.CENTER);

    names = new JComboBox<>();
    for (int i = 0; i < ContactsList.getSize(); i++) {
      names.addItem(ContactsList.getName(i));
    }

    add(names);

    add(new JLabel("Enter Name:"));
    add(name = new JTextField(15));
    add(new JLabel("Enter Phone Number:"));
    add(phone = new JTextField(15));
    add(new JLabel("Enter Email:"));
    add(email = new JTextField(15));
    add(new JLabel("Ente DOB:"));
    add(dob = new JTextField(15));

    add(update = new JButton("UPDATE"));
    update.addActionListener(this);
    names.addItemListener(this);
  }

  public void actionPerformed(ActionEvent ae) {
    String deleteData = names.getSelectedItem().toString();
    if (deleteData == "") {
      JOptionPane.showMessageDialog(this, "Please select a contact to be updated", "ERROR!", JOptionPane.ERROR_MESSAGE);
      
    } else {
      ContactsList.updateContact(deleteData, name.getText(), phone.getText(), email.getText(), dob.getText());
      JOptionPane.showMessageDialog(this, "Contact Updated Successfully!", "SUCCESS!", JOptionPane.INFORMATION_MESSAGE);
      obj.updateContactsTab();
      obj.updateContactsTab();
      name.setText("");
      phone.setText("");
      email.setText("");
      dob.setText("");
    }
  }

  public void itemStateChanged(ItemEvent ie) {
    Contact c = ContactsList.getContact(names.getSelectedIndex());
    name.setText(c.getName());
    phone.setText(c.getPhoneNumber());
    email.setText(c.getEmail());
    dob.setText(c.getDOB());
  }
}
