import javax.swing.*;
import java.awt.*;

public class GUI extends JFrame {
  JTabbedPane jtp;
  GUI() {
    setSize(700, 400);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    Container contentPane = getContentPane();
    jtp = new JTabbedPane(JTabbedPane.TOP);
    jtp.add("CONTACTS", new ContactsTab());
    jtp.add("ADD", new AddTab(this));
    jtp.add("UPDATE", new UpdateTab(this));
    jtp.add("DELETE", new DeleteTab(this));
    contentPane.add(jtp);

    setVisible(true);
  }

  void updateContactsTab() {
    jtp.setComponentAt(0, new ContactsTab());
    jtp.setComponentAt(1, new AddTab(this));
    jtp.setComponentAt(2, new UpdateTab(this));
    jtp.setComponentAt(3, new DeleteTab(this));
  }
}
